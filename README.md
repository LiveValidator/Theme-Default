# LiveValidator - Theme Default

[![build status](https://gitlab.com/LiveValidator/Theme-Default/badges/master/build.svg)](https://gitlab.com/LiveValidator/Theme-Default/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Theme-Default/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Theme-Default/commits/master)

Simple theme for LiveValidator that only needs vanilla JS and a small Stylesheet.

Find the project [home page and docs](https://chesedo.gitlab.io/LiveValidator/).
