var theme = theme || {};

theme.markRequiredSpec = function() {
    beforeEach( function() {
        this.row = helper.themes.getRow();
        this.theme = new LiveValidator.themes.Default( helper.themes.getInput( this.row ) );
    } );

    it( 'already required', function() {
        this.row.classList.add( 'required' );
        expect( this.row ).toHaveClass( 'required' );
        this.theme.markRequired();
        expect( this.row ).toHaveClass( 'required' );
    } );

    it( 'not required', function() {
        expect( this.row ).not.toHaveClass( 'required' );
        this.theme.markRequired();
        expect( this.row ).toHaveClass( 'required' );
    } );
};
