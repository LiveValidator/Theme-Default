var theme = theme || {};

theme.unmarkRequiredSpec = function() {
    beforeEach( function() {
        this.row = helper.themes.getRow();
        this.theme = new LiveValidator.themes.Default( helper.themes.getInput( this.row ) );
    } );

    it( 'already required', function() {
        this.row.classList.add( 'required' );
        expect( this.row ).toHaveClass( 'required' );
        this.theme.unmarkRequired();
        expect( this.row ).not.toHaveClass( 'required' );
    } );

    it( 'not required', function() {
        expect( this.row ).not.toHaveClass( 'required' );
        this.theme.unmarkRequired();
        expect( this.row ).not.toHaveClass( 'required' );
    } );
};
