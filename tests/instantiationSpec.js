var theme = theme || {};

theme.instantiationSpec = function() {
    function getBareInput() {
        setFixtures( '<input />' );
        return document.getElementsByTagName( 'input' )[ 0 ];
    }

    beforeEach( function() {
        this.options = {
            error: 'error',
            missing: 'missing',
            parentSelector: '.row'
        };
    } );

    it( 'when called without `new`', function() {
        var instance = LiveValidator.themes.Default( getBareInput() );

        expect( instance.options ).toBeDefined();
        expect( window.options ).toBeUndefined();
    } );

    it( 'when called without options', function() {
        var instance = new LiveValidator.themes.Default( getBareInput() );

        expect( instance.options ).toEqual( this.options );
    } );

    it( 'when called with options', function() {
        var instance = new LiveValidator.themes.Default( getBareInput(), { error: 'fail' } );

        this.options.error = 'fail';

        expect( instance.options ).toEqual( this.options );
    } );
};
