/**
 * The test suite for the default theme "class" (Default.js)
 */

/* globals theme */
describe( 'Default theme', function() {
    describe( 'check instantiation', function() {
        theme.instantiationSpec();
    } );
    describe( 'check `markRequired` when', function() {
        theme.markRequiredSpec();
    } );
    describe( 'check `unmarkRequired` when', function() {
        theme.unmarkRequiredSpec();
    } );
    describe( 'check `setMissing` when', function() {
        theme.setMissingSpec();
    } );
    describe( 'check `unsetMissing` when', function() {
        theme.unsetMissingSpec();
    } );
    describe( 'check `addErrors` when', function() {
        theme.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        theme.clearErrorsSpec();
    } );

    it( 'sets itself as the default theme', function() {
        expect( LiveValidator.defaults.theme ).toBeDefined();
        expect( LiveValidator.defaults.theme ).toBe( LiveValidator.themes.Default );
    } );
} );
